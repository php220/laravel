<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class GettingWeatherController extends Controller
{
    public function weatherGet($nameCity)
    {
        Artisan::call("command:gettingweather", ["nameCity" => $nameCity]);
        $weather = Artisan::output();
        return response()->view("weather.weather", ["weather" => $weather]);
    }

    public function indexWeather()
    {
        return response()->view("weather.weatherpost");
    }

    public function weatherPost(Request $request)
    {
        $data = $request->except("_token");
        Artisan::call("command:gettingweather", ["nameCity" => $data["city"]]);
        $weather = Artisan::output();
        return response()->view("weather.weather", ["weather" => $weather]);
    }
}
