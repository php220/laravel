<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GettingWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:gettingweather {nameCity}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Can getting weather in different city';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /*$nameCity = $this->argument('nameCity');
        $apiKey = $_ENV["OPEN_WEATHER_MAP_API_KEY"];

        $ch = curl_init("https://api.openweathermap.org/data/2.5/weather?q={$nameCity}&appid={$apiKey}&units=metric");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $html = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($html);
        $temperature = $data->main->temp;
        $description = $data->weather[0]->description;

        $this->line("Current weather in {$nameCity}: {$temperature}°C, {$description}");
        $this->info('The command was successful!');*/

        $nameCity = $this->argument('nameCity');
        $apiKey = $_ENV["OPEN_WEATHER_MAP_API_KEY"];
        $url = "https://api.openweathermap.org/data/2.5/weather?q={$nameCity}&appid={$apiKey}&units=metric";
        $response = Http::get($url);

        $data = json_decode($response->body());

        $temperature = $data->main->temp;
        $description = $data->weather[0]->description;
        $this->info("Current weather in {$nameCity}: {$temperature}°C, {$description}");
    }
}
