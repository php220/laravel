<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\AdminProductController;
use App\Http\Controllers\GettingWeatherController;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    //return "Hello World!";
});

Route::post('/', function () {
    //return view('welcome');
    return "Hello World!";
});

Route::get('/test/{id}', [TestController::class, "index"]);

Route::prefix("admin")->group(function () {
    Route::resource("products", AdminProductController::class);
});

Route::prefix("weather")->group(function () {
    Route::get('/get/{nameCity}', [GettingWeatherController::class, "weatherGet"]);
    Route::get('/weatherpost', [GettingWeatherController::class, "indexWeather"]);
    Route::post('/post', [GettingWeatherController::class, "weatherPost"]);
});
