<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="/weather/post" method="post">
    @csrf
    <label for="city">Enter a city name:</label>
    <input class="city" type="text" name="city" required>
    <button type="submit">Get Weather</button>
</form>
</body>
</html>
